package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type Painting struct {
	Id    primitive.ObjectID `json:",string" bson:"_id,omitempty"`
	Name  string             `json:"name"`
	Price float32            `json:"price"`
	Owner Owner              `json:"owner"`
}
