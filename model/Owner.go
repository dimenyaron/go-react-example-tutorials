package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type Owner struct {
	Id        primitive.ObjectID `json:",string" bson:"_id,omitempty"`
	FirstName string             `json:"firstName"`
	LastName  string             `json:"lastName"`
	Age       int                `json:"age"`
}
