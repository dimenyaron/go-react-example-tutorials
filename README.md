# Go-React example + tutorials



## Running the app  

Frontend:  
 - cd ./frontend
 - yarn
 - yarn start  

Backend:
 - go run ./Main.go   (don't forget to start up a MongoDB database on the default port)

## Tutorials

Go:  
 - tutorial video: https://www.youtube.com/watch?v=YS4e4q9oBaU  
 - playground: https://golang.org/
 - documentation: https://golang.org/doc/

React:
 - tutorial video: https://www.youtube.com/watch?v=Ke90Tje7VS0&t=1161s
 - react hooks: https://reactjs.org/docs/hooks-overview.html

Typescript:
 - https://www.typescriptlang.org/

SemanticUI React:
 - https://react.semantic-ui.com/
