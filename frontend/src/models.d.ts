interface Owner  {
    Id: string
    firstName: string
    lastName: string
    age: int
}

interface Painting {
    Id: string
    name: string
    price: int
    owner: Owner
}