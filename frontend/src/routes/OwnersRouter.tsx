import React, { useEffect, useState } from 'react'
import OwnerItem from '../components/OwnerItem'
import { getOwnerById, getOwnersByName } from '../API'
import { useParams } from 'react-router-dom'
import { Input } from 'semantic-ui-react'


const Owners: React.FC = (props) => {
  const [owners, setOwners] = useState<Owner[]>([])
  const [loaded, setLoaded] = useState<boolean>(false)
  const [primarySearch, setPrimarySearch] = useState<string>('')

  useEffect(() => {
    
    const params = new URLSearchParams(window.location.search)
    let search = params.get('search')
    fetchOwners(search)

  })

  const { id } = useParams<{ id: string }>();

  const setState = (owner:Owner) => {
    let owners : Owner[] = []
    owners.push(owner)
    setOwners(owners)
  }

  const back = () => {
    setLoaded(false)
  }

  const fetchOwners = (search : string|null): void => {
    if(!loaded && id === undefined && primarySearch === ''){
            getOwnersByName(search)
            .then((data: Owner[] | any) => {setOwners(data);setLoaded(true);})
            .catch((err: Error) => console.log(err))
    }else if(!loaded && id === undefined){
      getOwnersByName(primarySearch)
            .then((data: Owner[] | any) => {setOwners(data);setLoaded(true);})
            .catch((err: Error) => console.log(err))
    } else if(!loaded){
      getOwnerById(id)
            .then((data: Owner | any) => {let owners: Owner[] = [data]; setOwners(owners);setLoaded(true);})
            .catch((err: Error) => console.log(err))
    }
  }
  
  return (
      <div>
      <h2>My Owners</h2>
      <Input onChange={e => {setLoaded(false);setPrimarySearch(e.target.value)}} placeholder='search'/>
            {owners.map((owner: Owner) => (
            <OwnerItem
              key={owner.Id}
              owner={owner}
              setOwnersOfParent={setState}
              back={back}
            />
          ))}
      </div>
  )
}

export default Owners