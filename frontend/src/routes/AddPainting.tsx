import { useForm } from "react-hook-form"
import { useHistory } from "react-router"
import { Button, Form, FormField, Input } from "semantic-ui-react"
import { addPainting } from "../API"

const AddPainting :React.FC =() => {

    let history = useHistory()

    const {register,handleSubmit}=useForm<Painting>();
    const onSubmit = ((data:Painting):void => {
        addPainting(data).then()
        history.push("/paintings")
    })

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <FormField>
                <Input label="Name" required type="text" {...register("name")}/>
            </FormField>
            <FormField>
                <Input label="Price" required type="float" {...register("price")}/>
            </FormField>
            <FormField>
                <Input label="Owner ID" required type="text" {...register("owner.Id")}/>
            </FormField>
            <Button type="submit"> Submit </Button>
        </Form>
    )
}

export default AddPainting