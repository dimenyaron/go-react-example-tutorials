import { useForm } from "react-hook-form"
import { useHistory } from "react-router"
import { Button, Form, FormField, Input } from "semantic-ui-react"
import { addOwner } from "../API"

const AddOwner :React.FC =() => {

    let history = useHistory()

    const {register,handleSubmit}=useForm<Owner>();
    const onSubmit = ((data:Owner):void => {
        addOwner(data).then()
        history.push("/owners")
    })

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <FormField>
                <Input label="First Name" required type="text" {...register("firstName")}/>
            </FormField>
            <FormField>
                <Input label="Last Name" required type="text" {...register("lastName")}/>
            </FormField>
            <FormField>
                <Input label="Age" required type="number" {...register("age")}/>
            </FormField>
            <Button type="submit"> Submit </Button>
        </Form>
    )
}

export default AddOwner