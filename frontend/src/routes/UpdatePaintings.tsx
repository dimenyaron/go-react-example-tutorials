import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { useHistory } from "react-router"
import { useParams } from "react-router-dom"
import { Button, Form, FormField, Input } from "semantic-ui-react"
import { getPaintingById, updatePainting } from "../API"

const UpdatePainting :React.FC =() => {

    const [painting,setPainting] = useState<Painting>()
    const [loaded, setLoaded] = useState<boolean>(false)

    const { id } = useParams<{ id: string }>();

    useEffect(() => {
        fetchPainting()
      })

      const fetchPainting = (): void => {
       if(!loaded){
            getPaintingById(id)
                .then((data: Painting) => {setPainting(data);setLoaded(true);})
                .catch((err: Error) => console.log(err))
        }
      }

    let history = useHistory()

    const {register,handleSubmit}=useForm<Painting>();
    const onSubmit = ((data:Painting):void => {
        updatePainting(painting,data).then()
        history.push("/paintings/")
    })

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <FormField>
                <Input placeholder={painting?.name} label="Name" type="text" {...register("name")}/>
            </FormField>
            <FormField>
                <Input placeholder={painting?.price} label="Price" type="float" {...register("price")}/>
            </FormField>
            <FormField>
                <Input placeholder={painting?.owner.Id} label="Owner ID" type="number" {...register("owner.Id")}/>
            </FormField>
            <Button type="submit"> Submit </Button>
        </Form>
    )
}

export default UpdatePainting