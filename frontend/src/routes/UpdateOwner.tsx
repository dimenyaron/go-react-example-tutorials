import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { useHistory } from "react-router"
import { useParams } from "react-router-dom"
import { Button, Form, FormField, Input } from "semantic-ui-react"
import { getOwnerById, updateOwner } from "../API"

const UpdateOwner :React.FC =() => {

    const [owner, setOwner] = useState<Owner>()
    const [loaded, setLoaded] = useState<boolean>(false)

    const { id } = useParams<{ id: string }>();

    useEffect(() => {
        fetchOwner()
      })

      const fetchOwner = (): void => {
       if(!loaded){
          getOwnerById(id)
                .then((data: Owner | any) => {setOwner(data);setLoaded(true);})
                .catch((err: Error) => console.log(err))
        }
      }

    let history = useHistory()

    const {register,handleSubmit}=useForm<Owner>();
    const onSubmit = ((data:Owner):void => {
        updateOwner(owner,data).then()
        history.push("/owners/")
    })

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <FormField>
                <Input placeholder={owner?.firstName} label="First Name" type="text" {...register("firstName")}/>
            </FormField>
            <FormField>
                <Input placeholder={owner?.lastName} label="Last Name" type="text" {...register("lastName")}/>
            </FormField>
            <FormField>
                <Input placeholder={owner?.age} label="Age" type="number" {...register("age")}/>
            </FormField>
            <Button type="submit"> Submit </Button>
        </Form>
        
    )
}

export default UpdateOwner