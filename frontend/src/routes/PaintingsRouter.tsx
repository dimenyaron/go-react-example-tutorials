import React, { useEffect, useState } from 'react'
import PaintingItem from '../components/PaintingItem'
import { getPaintingsByName, getPaintingsOfOwner } from '../API'
import { Input } from 'semantic-ui-react'
import { useParams } from 'react-router-dom'

interface IPaintingsProps {
  pid ?: string
}

const Paintings: React.FC<IPaintingsProps> = (props) => {
  const [paintings, setPaintings] = useState<Painting[]>([])
  const [loaded, setLoaded] = useState<boolean>(false)
  const [primarySearch, setPrimarySearch] = useState<string>('')

  useEffect(() => {
    const params = new URLSearchParams(window.location.search)
    let search = params.get('search')
    fetchPaintings(search)
  })

  const { id } = useParams<{ id: string }>();

  const fetchPaintings = (search: string|null): void => {
    if(!loaded && id === undefined && primarySearch === ''){
      getPaintingsByName(search)
      .then((data: Painting[] | any) => {setPaintings(data);setLoaded(true)})
      .catch((err: Error) => console.log(err))
    }else if (!loaded && id === undefined){
      getPaintingsByName(primarySearch)
      .then((data: Painting[] | any) => {setPaintings(data);setLoaded(true)})
      .catch((err: Error) => console.log(err))
    }
     else if(!loaded) {
      getPaintingsOfOwner(id)
      .then((data: Painting[] | any) => {setPaintings(data);setLoaded(true)})
      .catch((err: Error) => console.log(err))
    }
  }

  const setState = (painting: Painting) => {
    let paintings : Painting[] = []
    paintings.push(painting)
    setPaintings(paintings)
  }

  const back = () => {
    setLoaded(false)
  }

  return (
      <div>
      <h2>My Paintings</h2>
      <Input onChange={e => {setLoaded(false);setPrimarySearch(e.target.value)}} placeholder='search'/>
      {paintings.map((painting: Painting) => (
        <PaintingItem
          key={painting.Id}
          painting={painting}
          setPaintingsOfParent={setState}
          back={back}
        />
      ))}
      </div>
  )
}

export default Paintings