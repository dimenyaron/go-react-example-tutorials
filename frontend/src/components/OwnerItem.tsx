import React from "react";
import { Container } from "semantic-ui-react"
import { Menu } from "semantic-ui-react"
import {
    BrowserRouter as Router,
    NavLink,
    Route,
    Switch,
    useHistory
  } from "react-router-dom";
import PaintingsRouter from "../routes/PaintingsRouter";
import { deleteOwner } from "../API";



interface OwnerProps {
    owner: Owner
    setOwnersOfParent: (owner: Owner) => void
    back: () => void
}

const OwnerItem: React.FC<OwnerProps> = ({owner,setOwnersOfParent,back}) => {

    let history = useHistory()

    return (
        <Container>
            <p>
                Id: {owner.Id}
                <br></br>
                First Name: {owner.firstName}
                <br></br>
                Last Name: {owner.lastName}
                <br></br>
                Age: {owner.age}
                <Router>
                <Menu color="red">
                    <Menu.Item
                    name='owners'
                    >
                    <NavLink to={"/owners/"+owner.Id} onClick={()=>setOwnersOfParent(owner)}> Details </NavLink>
                    </Menu.Item>
                    <br/>
                    <Menu.Item
                    name='paintings'
                    >
                    <NavLink to="/paintings" > Paintings </NavLink>
                    
                    </Menu.Item>

                    <Menu.Item
                    name='refresh'
                    >
                    <NavLink to="/owners" onClick={()=> back()}> Back </NavLink>
                    
                    </Menu.Item>
                    <Menu.Item name="deleteOwner">
                    <NavLink to="/owners" onClick={()=> {deleteOwner(owner.Id); back();}}> Delete </NavLink> 
                    </Menu.Item>
                    
                    <Menu.Item name="updateOwner">
                    <NavLink to={"/updateOwner/"+owner.Id} onClick={()=>{history.push("/updateOwner/"+owner.Id)}}>  Update </NavLink> 
                    </Menu.Item>

                    <Switch>
                        <Route path="/paintings/" children={<PaintingsRouter pid={owner.Id}/>} />
                    </Switch>
                </Menu>
                </Router>
            </p>
        </Container>
    )
}

export default OwnerItem