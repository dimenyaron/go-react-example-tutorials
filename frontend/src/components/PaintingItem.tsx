import React from "react";
import { NavLink, useHistory } from "react-router-dom";
import { Container, Menu } from "semantic-ui-react"
import { deletePainting } from "../API";

interface PaintingProps {
    painting: Painting
    setPaintingsOfParent: (painting: Painting) => void
    back: () => void
}

const PaintingItem: React.FC<PaintingProps> = ({setPaintingsOfParent,painting,back}) => {

    let history = useHistory()

    return (
        <Container>
            <p>
                Id: {painting.Id}
                <br></br>
                Name: {painting.name}
                <br></br>
                Price: {painting.price}
                <br></br>
                Owner: 
                <br></br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Id: {painting.owner.Id}
                <br></br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; First Name: {painting.owner.firstName}
                <br></br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Last Name: {painting.owner.lastName}
                <br></br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Age: {painting.owner.age}
                <Menu color="red">
                    <Menu.Item
                    name='owners'
                    >
                    <NavLink to={"/paintings/"+painting.Id} onClick={()=>setPaintingsOfParent(painting)}> Details </NavLink>
                    </Menu.Item>
                    <br/>

                    <Menu.Item
                    name='refresh'
                    >
                    <NavLink to="/paintings" onClick={()=> back()}> Back </NavLink>
                    
                    </Menu.Item>
                    <Menu.Item name="deleteOwner">
                    <NavLink to="/paintings" onClick={()=> {deletePainting(painting.Id); back();}}> Delete </NavLink> 
                    </Menu.Item>

                    <Menu.Item name="updatePainting">
                    <NavLink to={"/updatePainting/"+painting.Id} onClick={()=>{history.push("/updatePainting/"+painting.Id)}}>  Update </NavLink> 
                    </Menu.Item>
                </Menu>
            </p>
        </Container>
    )
}

export default PaintingItem