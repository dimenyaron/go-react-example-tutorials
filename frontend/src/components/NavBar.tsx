import { useState } from "react"
import { Menu } from "semantic-ui-react"
import {
    BrowserRouter,
    Switch,
    Route,
    NavLink,
  } from "react-router-dom";
  import OwnersRouter from "../routes/OwnersRouter"
import PaintingsRouter from "../routes/PaintingsRouter";
import AddOwner from "../routes/AddOwner";
import AddPainting from "../routes/AddPainting";
import UpdateOwner from "../routes/UpdateOwner";
import UpdatePainting from "../routes/UpdatePaintings";

const NavBar: React.FC = () => {
    const [activeItem, setActiveItem] = useState<string>('')

    return (
      <BrowserRouter>
        <Menu>
        <Menu.Item
          name='owners'
          active={activeItem === 'owners'}
          onClick={() => setActiveItem('owners')}
        >
         <NavLink to="/owners"> Owners </NavLink>
        </Menu.Item>
        <br/>
        <Menu.Item
          name='paintings'
          active={activeItem === 'paintings'}
          onClick={() => {setActiveItem('paintings')}}
        >
          <NavLink to="/paintings"> Paintings </NavLink>
        </Menu.Item>
        <Menu.Item
          name='addowners'
          active={activeItem === 'addowners'}
          onClick={() => setActiveItem('addowners')}
        >
         <NavLink to="/addOwner"> Add owner </NavLink>
        </Menu.Item>
        <Menu.Item
          name='addpaintings'
          active={activeItem === 'addpaintings'}
          onClick={() => setActiveItem('addowners')}
        >
         <NavLink to="/addPainting"> Add painting </NavLink>
        </Menu.Item>
      </Menu>
      <Switch>
        <Route path="/owners/:id">
          <OwnersRouter />
        </Route>
        <Route path="/owners/">
          <OwnersRouter />
        </Route>
        <Route path="/paintings/:id">
          <PaintingsRouter />
        </Route>
        <Route path="/paintings">
          <PaintingsRouter />
        </Route>
        <Route path="/test">
          <h1>Ez egy test</h1>
        </Route>
        <Route path="/addOwner">
          <AddOwner />
        </Route>
        <Route path="/addPainting">
          <AddPainting />
        </Route>
        <Route path="/updateOwner/:id">
          <UpdateOwner />
        </Route>
        <Route path="/updatePainting/:id">
          <UpdatePainting />
        </Route>
      </Switch>
      </BrowserRouter>
    )
}

export default NavBar