import React from "react";
import NavBar from "./components/NavBar";
import { Header } from "semantic-ui-react";

const App: React.FC = () => {
    const styleLink = document.createElement("link");
    styleLink.rel = "stylesheet";
    styleLink.href = "https://cdn.jsdelivr.net/npm/semantic-ui/dist/semantic.min.css";
    document.head.appendChild(styleLink);

    return (
        <main className="App">
            <Header as="h1">Welcome to the site!</Header>
            <NavBar />
        </main>
    );
};

export default App;
