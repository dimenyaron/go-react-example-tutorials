import axios from "axios";

const baseUrl: string = "http://localhost:8080/api"

export const getOwners = async (): Promise<Owner[]> => {
    try {
      const owners: Owner[] = await axios.get(
        baseUrl + "/owners/", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then((res) => {return res.data});
      if(owners === null){
        return []
      }
      return owners
    } catch (error) {
      throw new Error("error")
    }
  }

  export const getPaintings = async (): Promise<Painting[]> => {
    try {
      const paintings: Painting[] = await axios.get(
        baseUrl + "/paintings/", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then((res) => {return res.data});
        if (paintings === null){
          return []
        }
      return paintings
    } catch (error) {
      throw new Error("error")
    }
  }

  export const getOwnerById = async (id:string): Promise<Owner> => {
    try {
      const owners: Owner = await axios.get(
        baseUrl + "/owners/"+id+"", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then((res) => {return res.data});
      return owners
    } catch (error) {
      throw new Error("error")
    }
  }

  export const getPaintingById = async (id:string): Promise<Painting> => {
    try {
      const paintings: Painting = await axios.get(
        baseUrl + "/paintings/"+id+"", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then((res) => {return res.data});
      return paintings
    } catch (error) {
      throw new Error("error")
    }
  }

  export const getPaintingsByName = async (name: string|null): Promise<Painting[]> => {
    try {
      if(name === null){
        try {
          const paintings: Painting[] = await axios.get(
            baseUrl + "/paintings/", {
              headers: {
                "Access-Control-Allow-Origin": "http://localhost",
                "Access-Control-Allow-Methods": "*",
                "Access-Control-Allow-Headers": "*"
                      }
            }).then((res) => {return res.data});
            if (paintings === null){
              return []
            }
          return paintings
          
        } catch (error) {
          throw new Error("error")
        }
      }
      const paintings: Painting[] = await axios.get(
        baseUrl + "/paintings/?search="+name+"/", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then((res) => {return res.data});
        if (paintings === null){
          return []
        }
      return paintings
    } catch (error) {
      throw new Error("error")
    }
  }

  export const getOwnersByName = async (name: string|null): Promise<Owner[]> => {
    try {
      if(name === null){
        try {
          const owners: Owner[] = await axios.get(
            baseUrl + "/owners/", {
              headers: {
                "Access-Control-Allow-Origin": "http://localhost",
                "Access-Control-Allow-Methods": "*",
                "Access-Control-Allow-Headers": "*"
                      }
            }).then((res) => {return res.data});
            if(owners === null){
              return []
            }
          return owners
        } catch (error) {
          throw new Error("error")
        }
      }
      const owners: Owner[] = await axios.get(
        baseUrl + "/owners/?search="+name+"", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then((res) => {return res.data});
        if(owners === null){
          return []
        }
      return owners
    } catch (error) {
      throw new Error("error")
    }
  }

  export const getPaintingsOfOwner = async (id: string|undefined): Promise<Painting[]> => {
    try {
      const paintings: Painting[] = await axios.get(
        baseUrl + "/owners/"+id+"/paintings", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then((res) => {return res.data});
      if (paintings === null){
        return []
      }
      return paintings
      
    } catch (error) {
      throw new Error("error")
    }
  }

  export const deleteOwner = async (id: string|undefined): Promise<string> => {
    try {
      const retVal: string = await axios.delete(
        baseUrl + "/owners/"+id+"", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then(()=>{return "ok"}).catch((err)=>{return "error"})
        if(retVal === null){
          return "error"
        }
        return retVal
    } catch (error) {
      throw new Error("error")
    }
  }

  export const deletePainting = async (id: string|undefined): Promise<string> => {
    try {
      const retVal: string = await axios.delete(
        baseUrl + "/paintings/"+id+"", {
          headers: {
            "Access-Control-Allow-Origin": "http://localhost",
            "Access-Control-Allow-Methods": "*",
            "Access-Control-Allow-Headers": "*"
                  }
        }).then(()=>{return "ok"}).catch((err)=>{return "error"})
        if(retVal === null){
          return "error"
        }
        return retVal
    } catch (error) {
      throw new Error("error")
    }
  }

  export const addOwner = async (owner: Owner): Promise<string> => {
    try {

          owner.age=parseInt(owner.age)
          const retVal: string = await axios.post(
            baseUrl + "/owners/",       
                {
                  firstName: owner.firstName,
                  lastName: owner.lastName,
                  age: owner.age
                }
            ).then(()=>{return "ok"}).catch((err)=>{return "error"})
            if(retVal === null){
              return "error"
            }
            return retVal
        } catch (error) {
          throw new Error("error")
        }
      }

      export const addPainting = async (painting:Painting): Promise<string> => {
        try {
    
              painting.price=parseFloat(painting.price)
              const owner = await getOwnerById(painting.owner.Id)
              .then((data: Owner | any) => {return data})
              .catch((err: Error) => console.log(err))
              const retVal: string = await axios.post(
                baseUrl + "/paintings/",       
                    {
                      name: painting.name,
                      price: painting.price,
                      owner: owner
                    }
                ).then(()=>{return "ok"}).catch((err)=>{return "error"})
                if(retVal === null){
                  return "error"
                }
                return retVal
            } catch (error) {
              throw new Error("error")
            }
          }
    export const updateOwner = async (owner: Owner|undefined, newOwner: Owner): Promise<string> => {
      if(owner === undefined){
        throw new Error("error")
      }
     try {
        if(newOwner.firstName !== '' && newOwner.firstName !== undefined && newOwner.firstName !== null ){
                    owner.firstName=newOwner.firstName
                  }
                  if(newOwner.lastName !== '' && newOwner.lastName !== undefined && newOwner.lastName !== null ){
                    owner.lastName=newOwner.lastName
                  }
                  if(newOwner.age !== 0 && newOwner.age !== undefined && newOwner.age !== null ){
                    owner.age=newOwner.age
                  }
                  owner.age=parseInt(owner.age)
                  const retVal: string = await axios.patch(
                    baseUrl + "/owners/"+owner.Id + "",       
                        {
                          firstName: owner.firstName,
                          lastName: owner.lastName,
                          age: owner.age
                        }
                    ).then(()=>{return "ok"}).catch((err)=>{return "error"})
                    if(retVal === null){
                      return "error"
                    }
                    return retVal
                } catch (error) {
                  throw new Error("error")
                }
              }
              
              export const updatePainting = async (painting: Painting|undefined, newPainting:Painting): Promise<string> => {
                if(painting === undefined){
                  throw new Error("error")
                }
               try {
                  if(newPainting.name !== '' && newPainting.name !== undefined && newPainting.name !== null ){
                              painting.name=newPainting.name
                            }
                            if(newPainting.price !== 0 && newPainting.price !== undefined && newPainting.price !== null ){
                              painting.price=newPainting.price
                            }
                            if(newPainting.owner.Id !== '' && newPainting.owner.Id !== undefined && newPainting.owner.Id !== null ){
                              painting.owner.Id=newPainting.owner.Id
                            }
                            painting.price=parseFloat(painting.price)
                            const owner = await getOwnerById(painting.owner.Id)
                            .then((data: Owner | any) => {return data})
                            .catch((err: Error) => {console.log(err);return null})
                            if(owner === null){
                              return "error"
                            }
                            const retVal: string = await axios.patch(
                              baseUrl + "/paintings/" + painting.Id,       
                                {
                                  name: painting.name,
                                  price: painting.price,
                                  owner: owner
                                }
                              ).then(()=>{return "ok"}).catch((err)=>{return "error"})
                              if(retVal === null){
                                return "error"
                              }
                              return retVal
                          } catch (error) {
                            throw new Error("error")
                          }
                        }