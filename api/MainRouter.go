package api

import (
	"GoLangExample/api/routes"

	"github.com/gin-gonic/gin"
)

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "*")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func StartListening() {
	var router = gin.New()
	// router.Use(cors.Middleware(cors.Config{
	// 	Origins:         "::1",
	// 	Methods:         "*",
	// 	RequestHeaders:  "*",
	// 	ExposedHeaders:  "",
	// 	MaxAge:          50 * time.Second,
	// 	Credentials:     false,
	// 	ValidateHeaders: false,
	// }))
	router.OPTIONS("/", func(c *gin.Context) {
		c.Header("Access-Control-Allow-Origin", "http://localhost")
		c.Header("Access-Control-Allow-Credentials", "*")
		c.Header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE")
		c.Header("Access-Control-Allow-Headers", "*")
		c.JSON(200, "")
	})
	router.Use(CORSMiddleware())
	var apiRouter = router.Group("/api")
	routes.OwnerRoutes(apiRouter)
	routes.PaintingRoutes(apiRouter)
	router.Run()
}
