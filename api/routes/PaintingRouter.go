package routes

import (
	"GoLangExample/model"
	"GoLangExample/repository"
	"encoding/json"
	"io/ioutil"

	"github.com/gin-gonic/gin"
)

func PaintingRoutes(route *gin.RouterGroup) {
	paintingRouter := route.Group("/paintings")

	paintingRouter.GET("/:id", func(c *gin.Context) {
		id := c.Param("id")
		painting, err := repository.FindPaintingByID(id)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(200, painting)
	})

	paintingRouter.GET("/", func(c *gin.Context) {
		name := c.DefaultQuery("search", "")
		var paintings []model.Painting
		var err error
		if name == "" {
			paintings, err = repository.FindAllPainting()
			if err != nil {
				c.JSON(500, gin.H{"message": err})
				return
			}
		} else {
			paintings, err = repository.FindPaintingsByName(name)
			if err != nil {
				c.JSON(500, gin.H{"message": err})
				return
			}
		}
		c.JSON(200, paintings)
	})

	paintingRouter.POST("/", func(c *gin.Context) {
		jsonData, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid painting data"})
			return
		}
		var paintingToInsert model.Painting
		err = json.Unmarshal(jsonData, &paintingToInsert)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid painting data"})
			return
		}
		if paintingToInsert.Owner.LastName == "" || paintingToInsert.Owner.FirstName == "" || paintingToInsert.Owner.Age < 1 || paintingToInsert.Name == "" || paintingToInsert.Price <= 0 {
			c.JSON(400, gin.H{"message": "Invalid painting data"})
			return
		}
		retVal, err := repository.InsertPainting(&paintingToInsert)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(200, retVal)
	})

	paintingRouter.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		err := repository.RemovePainting(id)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(204, "")
	})

	paintingRouter.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")
		jsonData, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid painting data"})
			return
		}
		var paintingNew model.Painting
		err = json.Unmarshal(jsonData, &paintingNew)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid painting data"})
			return
		}
		painting, err := repository.UpdatePainting(id, paintingNew)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(200, painting)
	})
}
