package routes

import (
	"GoLangExample/model"
	"GoLangExample/repository"
	"encoding/json"
	"io/ioutil"

	"github.com/gin-gonic/gin"
)

func OwnerRoutes(route *gin.RouterGroup) {
	ownerRouter := route.Group("/owners")

	ownerRouter.GET("/:id/paintings", func(c *gin.Context) {
		id := c.Param("id")
		paintings, err := repository.FindPaintingsOfOwner(id)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(200, paintings)
	})

	ownerRouter.GET("/:id", func(c *gin.Context) {
		id := c.Param("id")
		owner, err := repository.FindOwnerByID(id)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(200, owner)
	})

	ownerRouter.GET("/", func(c *gin.Context) {
		name := c.DefaultQuery("search", "")
		var owners []model.Owner
		var err error
		if name == "" {
			owners, err = repository.FindAllOwner()
			if err != nil {
				c.JSON(500, gin.H{"message": err})
				return
			}
		} else {
			owners, err = repository.FindOwnersByName(name)
			if err != nil {
				c.JSON(500, gin.H{"message": err})
				return
			}
		}
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
		// c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		// c.Writer.Header().Set("Access-Control-Allow-Headers", "*")
		// c.Writer.Header().Set("Access-Control-Allow-Methods", "GET")
		c.JSON(200, owners)
	})

	ownerRouter.POST("/", func(c *gin.Context) {
		jsonData, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid owner data"})
			return
		}
		var ownerToInsert model.Owner
		err = json.Unmarshal(jsonData, &ownerToInsert)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid owner data"})
			return
		}
		if ownerToInsert.LastName == "" || ownerToInsert.FirstName == "" || ownerToInsert.Age < 1 {
			c.JSON(400, gin.H{"message": "Invalid owner data"})
			return
		}
		retVal, err := repository.InsertOwner(&ownerToInsert)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(200, retVal)
	})

	ownerRouter.DELETE("/:id", func(c *gin.Context) {
		id := c.Param("id")
		err := repository.RemoveOwner(id)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(204, "")
	})

	ownerRouter.PATCH("/:id", func(c *gin.Context) {
		id := c.Param("id")
		jsonData, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid owner data"})
			return
		}
		var ownerNew model.Owner
		err = json.Unmarshal(jsonData, &ownerNew)
		if err != nil {
			c.JSON(400, gin.H{"message": "Invalid owner data"})
			return
		}
		owner, err := repository.UpdateOwner(id, ownerNew)
		if err != nil {
			c.JSON(500, gin.H{"message": err})
			return
		}
		c.JSON(200, owner)
	})
}
