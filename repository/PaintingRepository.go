package repository

import (
	model "GoLangExample/model"
	"errors"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func InsertPainting(painting *model.Painting) (model.Painting, error) {
	emptyOwner := model.Owner{}
	supposedOwner, err := FindOwnerByID(painting.Owner.Id.Hex())
	if painting.Owner == emptyOwner || painting.Owner != supposedOwner {
		return model.Painting{}, errors.New("owner is not valid")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	paintingCollection := client.Database("paintingDB").Collection("paintings")
	paintingData, err := bson.Marshal(painting)
	if err != nil {
		log.Panic(err)
		return model.Painting{}, errors.New("Cant convert painting data!")
	}
	res, err := paintingCollection.InsertOne(ctx, paintingData)
	if err != nil {
		log.Panic(err)
		return model.Painting{}, errors.New("Cant Insert painting data!")
	}
	painting.Id = res.InsertedID.(primitive.ObjectID)
	return *painting, nil
}

func FindAllPainting() ([]model.Painting, error) {
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	paintingCollection := client.Database("paintingDB").Collection("paintings")
	cursor, err := paintingCollection.Find(ctx, bson.M{})
	if err != nil {
		log.Panic(err)
		return []model.Painting{}, errors.New("Cant search paintings!")
	}
	var paintings []bson.M
	if err = cursor.All(ctx, &paintings); err != nil {
		log.Panic(err)
		return []model.Painting{}, errors.New("Cant search paintings!")
	}
	var retVal []model.Painting
	for _, value := range paintings {
		var o model.Painting
		bsonBytes, _ := bson.Marshal(value)
		bson.Unmarshal(bsonBytes, &o)
		retVal = append(retVal, o)
	}
	return retVal, nil
}

func FindPaintingByID(id string) (model.Painting, error) {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Panic(err)
		return model.Painting{}, errors.New("Cant convert Id!")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	paintingCollection := client.Database("paintingDB").Collection("paintings")
	var painting bson.M
	var retVal model.Painting
	if err := paintingCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&painting); err != nil {
		log.Panic(err)
		return model.Painting{}, errors.New("Cant search paintings")
	}
	bsonBytes, _ := bson.Marshal(painting)
	bson.Unmarshal(bsonBytes, &retVal)
	return retVal, nil
}

func FindPaintingsByName(s string) ([]model.Painting, error) {
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	paintingCollection := client.Database("paintingDB").Collection("paintings")
	var retVal []model.Painting
	filter := bson.M{"name": bson.M{
		"$regex": primitive.Regex{
			Pattern: s,
			Options: "i",
		},
	},
	}
	cursor, err := paintingCollection.Find(ctx, filter)
	if err != nil {
		log.Panic(err)
		return []model.Painting{}, errors.New("Cant search paintings!")
	}
	var paintings []bson.M
	if err = cursor.All(ctx, &paintings); err != nil {
		log.Panic(err)
		return []model.Painting{}, errors.New("Cant search paintings!")
	}
	for _, value := range paintings {
		var o model.Painting
		bsonBytes, _ := bson.Marshal(value)
		bson.Unmarshal(bsonBytes, &o)
		retVal = append(retVal, o)
	}
	return retVal, nil
}

func RemovePainting(id string) error {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Panic(err)
		return errors.New("Cant convert id!")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	paintingCollection := client.Database("paintingDB").Collection("paintings")
	_, err = paintingCollection.DeleteOne(ctx, bson.M{"_id": objId})
	if err != nil {
		log.Panic(err)
		return errors.New("Cant delete painting!")
	}
	return nil
}

func UpdatePainting(id string, painting model.Painting) (model.Painting, error) {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Panic(err)
		return model.Painting{}, errors.New("Cant convert Id!")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	paintingCollection := client.Database("paintingDB").Collection("paintings")
	paintingToUpdate, err := FindPaintingByID(id)
	if err != nil {
		return model.Painting{}, errors.New("Cant find painting!")
	}
	paintingData, err := bson.Marshal(painting)
	if err != nil {
		log.Panic(err)
		return model.Painting{}, errors.New("Cant convert painting data!")
	}
	if painting.Name == "" {
		painting.Name = paintingToUpdate.Name
	}
	if painting.Price == 0 {
		painting.Price = paintingToUpdate.Price
	}
	emptyOwner := model.Owner{}
	if painting.Owner != emptyOwner {
		supposedOwner, _ := FindOwnerByID(painting.Owner.Id.Hex())
		if painting.Owner != supposedOwner {
			return model.Painting{}, errors.New("Owner is not valid")
		}
	}
	if painting.Owner == emptyOwner {
		painting.Owner = paintingToUpdate.Owner
	}
	painting.Id = paintingToUpdate.Id
	_, err = paintingCollection.ReplaceOne(ctx, bson.M{"id": objId}, paintingData)
	if err != nil {
		log.Panic(err)
		return model.Painting{}, errors.New("Cant update!")
	}
	painting.Id = paintingToUpdate.Id
	return painting, nil
}

func FindPaintingsOfOwner(oid string) ([]model.Painting, error) {
	owner, err := FindOwnerByID(oid)
	if err != nil {
		return []model.Painting{}, errors.New("Cant find owner!")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	paintingCollection := client.Database("paintingDB").Collection("paintings")
	cursor, err := paintingCollection.Find(ctx, bson.M{"owner": owner})
	if err != nil {
		return []model.Painting{}, errors.New("Cant search paintings!")
	}
	var paintings []bson.M
	if err = cursor.All(ctx, &paintings); err != nil {
		log.Panic(err)
		return []model.Painting{}, errors.New("Cant search paintings!")
	}
	var retVal []model.Painting
	for _, value := range paintings {
		var o model.Painting
		bsonBytes, _ := bson.Marshal(value)
		bson.Unmarshal(bsonBytes, &o)
		retVal = append(retVal, o)
	}
	return retVal, nil
}
