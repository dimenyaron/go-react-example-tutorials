package repository

import (
	model "GoLangExample/model"
	"errors"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func InsertOwner(owner *model.Owner) (model.Owner, error) {
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	ownerCollection := client.Database("paintingDB").Collection("owners")
	ownerData, err := bson.Marshal(owner)
	if err != nil {
		log.Panic(err)
		return model.Owner{}, errors.New("Cant convert owner data")
	}
	res, err := ownerCollection.InsertOne(ctx, ownerData)
	if err != nil {
		log.Panic(err)
		return model.Owner{}, errors.New("Cant insert owner")
	}
	owner.Id = res.InsertedID.(primitive.ObjectID)
	return *owner, nil
}

func FindAllOwner() ([]model.Owner, error) {
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	ownerCollection := client.Database("paintingDB").Collection("owners")
	cursor, err := ownerCollection.Find(ctx, bson.M{})
	if err != nil {
		log.Panic(err)
		return []model.Owner{}, errors.New("Cant find owners")
	}
	var owners []bson.M
	if err = cursor.All(ctx, &owners); err != nil {
		log.Panic(err)
		return []model.Owner{}, err
	}
	var retVal []model.Owner
	for _, value := range owners {
		var o model.Owner
		bsonBytes, _ := bson.Marshal(value)
		bson.Unmarshal(bsonBytes, &o)
		retVal = append(retVal, o)
	}
	return retVal, nil
}

func FindOwnerByID(id string) (model.Owner, error) {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Panic(err)
		return model.Owner{}, errors.New("Cant convert id")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	ownerCollection := client.Database("paintingDB").Collection("owners")
	var owner bson.M
	var retVal model.Owner
	if err := ownerCollection.FindOne(ctx, bson.M{"_id": objId}).Decode(&owner); err != nil {
		log.Panic(err)
		return model.Owner{}, errors.New("Cant find owner")
	}
	bsonBytes, _ := bson.Marshal(owner)
	bson.Unmarshal(bsonBytes, &retVal)
	return retVal, nil
}

func FindOwnersByName(s string) ([]model.Owner, error) {
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	ownerCollection := client.Database("paintingDB").Collection("owners")
	var retVal []model.Owner
	filter := bson.M{"$or": []bson.M{{"firstname": bson.M{
		"$regex": primitive.Regex{
			Pattern: s,
			Options: "i",
		},
	},
	}, {"lastname": bson.M{
		"$regex": primitive.Regex{
			Pattern: s,
			Options: "i",
		},
	},
	}}}
	cursor, err := ownerCollection.Find(ctx, filter)
	if err != nil {
		log.Panic(err)
		return []model.Owner{}, errors.New("Cant find owners")
	}
	var owners []bson.M
	if err = cursor.All(ctx, &owners); err != nil {
		log.Panic(err)
		return []model.Owner{}, errors.New("Cant find owners")
	}
	for _, value := range owners {
		var o model.Owner
		bsonBytes, _ := bson.Marshal(value)
		bson.Unmarshal(bsonBytes, &o)
		retVal = append(retVal, o)
	}
	return retVal, nil
}

func RemoveOwner(id string) error {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Panic(err)
		return errors.New("Cant convert id")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	ownerCollection := client.Database("paintingDB").Collection("owners")
	_, err = ownerCollection.DeleteOne(ctx, bson.M{"_id": objId})
	if err != nil {
		log.Panic(err)
		return errors.New("Cant delete owner")
	}
	return nil
}

func UpdateOwner(id string, owner model.Owner) (model.Owner, error) {
	objId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Panic(err)
		return model.Owner{}, errors.New("Cant convert id")
	}
	client, ctx, cancel := Connect()
	defer cancel()
	defer Disconnect(client, ctx)
	ownerCollection := client.Database("paintingDB").Collection("owners")
	ownerToUpdate, err := FindOwnerByID(id)
	if err != nil {
		return model.Owner{}, err
	}
	if owner.Age == 0 {
		owner.Age = ownerToUpdate.Age
	}
	if owner.FirstName == "" {
		owner.FirstName = ownerToUpdate.FirstName
	}
	if owner.LastName == "" {
		owner.LastName = ownerToUpdate.LastName
	}
	owner.Id = ownerToUpdate.Id
	ownerData, err := bson.Marshal(owner)
	if err != nil {
		log.Panic(err)
		return model.Owner{}, errors.New("Cant convert owner data")
	}
	_, err = ownerCollection.ReplaceOne(ctx, bson.M{"_id": objId}, ownerData)
	if err != nil {
		log.Panic(err)
		return model.Owner{}, errors.New("Cant update owner")
	}
	return owner, nil
}
